var sensorsSource
var thermSource

function setupBrewserver() {
    sensorsSource = new EventSource('http://10.0.0.101:8080/sensors/stream')
    sensorsSource.onmessage = updateSensors

    thermSource = new EventSource('http://10.0.0.101:8080/thermostats/stream')
    thermSource.onmessage = updateTherms
}

function updateSensors(event) {
    var sensors = JSON.parse(event.data)['sensorData']

    for(var i = 0; i < sensors.length; i++) {
        $(`#sensor${sensors[i].sensor}TempF`).text(formatTempF(sensors[i].tempF))
    }
}

function updateTherms(event) {
    var therms = JSON.parse(event.data)['thermostatData']

    for(var i = 0; i < therms.length; i++) {
        if (!$(`#thermostat${therms[i].thermostat}Mode`).is(':focus'))
            $(`#thermostat${therms[i].thermostat}Mode`).val(therms[i].mode)

        $(`#thermostat${therms[i].thermostat}Status`).text(therms[i].status)

        $(`#thermostat${therms[i].thermostat}TargetSensor`).text(therms[i].targetSensor)

        if(!$(`#thermostat${therms[i].thermostat}TargetTemp`).is(':focus'))
            $(`#thermostat${therms[i].thermostat}TargetTemp`).val(therms[i].targetTemp.toFixed(0))

        $(`#thermostat${therms[i].thermostat}LimitSensor`).text(therms[i].limitSensor)

        if(!$(`#thermostat${therms[i].thermostat}LimitTemp`).is(':focus'))
            $(`#thermostat${therms[i].thermostat}LimitTemp`).val(therms[i].limitTemp.toFixed(0))
    }
}

function formatTempF(tempValue) {
    return `${tempValue.toFixed(1)} F`
}

function tempInputKeyUp(id, event) {
    /* if the enter key is pressed, cause the input to 'blur'
       triggering onchange */
    if(event.keyCode == 13) {
        event.preventDefault()
        $(`#${id}`).blur()
    }
}

function setThermostatTargetTemp(thermostat) {
    var temp = parseFloat($(`#thermostat${thermostat}TargetTemp`).val())
    console.log(`Setting target temp of ${thermostat} to ${temp}.`)

    $.ajax({
        url: `http://10.0.0.101:8080/thermostats/config/${thermostat}/target-temp/${temp}`,
        method: "PUT",
    }).fail(function(msg) {
        alert(`Couldn't set target temperature for ${thermostat}:\n${msg}`)
    })
}

function setThermostatLimitTemp(thermostat) {
    var temp = parseFloat($(`#thermostat${thermostat}LimitTemp`).val())
    console.log(`Setting limit temp of ${thermostat} to ${temp}.`)

    $.ajax({
        url: `http://10.0.0.101:8080/thermostats/config/${thermostat}/limit-temp/${temp}`,
        method: "PUT",
    }).fail(function(msg) {
        alert(`Couldn't set limit temperature for ${thermostat}:\n${msg}`)
    })
}

function setThermostatMode(thermostat) {
    var mode = $(`#thermostat${thermostat}Mode`).val()
    console.log(`Setting mode of ${thermostat} to ${mode}.`)

    $.ajax({
        url: `http://10.0.0.101:8080/thermostats/config/${thermostat}/mode/${mode.toLowerCase()}`,
        method: "PUT",
    }).fail(function(msg) {
        alert(`Couldn't set mode for ${thermostat}:\n${msg}`)
    })
}