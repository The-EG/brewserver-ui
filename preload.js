const electron = require('electron')

electron.contextBridge.exposeInMainWorld(
    'brewserver',
    {
        doClose: () => electron.remote.getCurrentWindow().close(),
        doMinimize: () => electron.remote.getCurrentWindow().minimize(),
        doMaximizeRestore: function () {
            var win = electron.remote.getCurrentWindow()

            if (win.isMaximized()) win.restore()
            else win.maximize()
        }
    }
)