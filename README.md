# The-EG's Brewserver

The Brewserver is a Raspberry Pi 3B that monitors and controls a fermentation environment (fancy term for a chest freezer) for fermenting home brewed beer.

## Brewserver-UI

This repository houses the UI written in Node.js and Electron. This is a cross platform UI that can be used to monitor and control the [Brewserver service control](https://gitlab.com/The-EG/brewserver-core) via the [Brewserver python web interface](https://gitlab.com/The-EG/brewserver-python).

TODO:
- The server url is hard coded. This should be configurable.

## Setup and Running

1. Clone this repository
2. From the root directory (containing this README.md), run `npm install`
3. Start the webserver from [brewserver-python](https://gitlab.com/The-EG/brewserver-python) if necessary: `python3 -m webserver`

To run it from the sources: `npm start`

To create an installer on Windows or a zip archive on other platforms: `npm run make`

